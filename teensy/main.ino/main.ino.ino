void readCommand(void);
void enterQueue(void);
void enterQueueOrgri(void);
void enterBG(void);
void emulateAVPlayer(void);
void leaveBG(void);
void randomCommand(void);
void hearthstone(void);
void jump(void);
void nonAFK();
void turnRight(void);
void turnLeft(void);
void getOutOfAVCave(void);
void goToFieldOfStrife(void);
void prepareAV(void);
void releaseSpirit(void);

IntervalTimer commandTimer;
IntervalTimer movementTimer;
IntervalTimer afkTimer;

// https://github.com/loglow/IntervalTimer
// max ~ 89.5" with default 48Mhz bus speed
long movementTimerMicros = 5000000;
long readCommandTimerMicros = 1000000;
long afkTimerMicros = 65000000; 

void setup() {
  Serial.begin(115200);
  Mouse.screenSize(1920, 1080);  
  commandTimer.begin(readCommand, readCommandTimerMicros);
}

void loop() {
}

void readCommand() {
    if (Serial.available()) {
        uint8_t incomingByte = Serial.read();
        switch(incomingByte) {
            case(0x31):
                enterQueue();
                afkTimer.end();
                afkTimer.begin(nonAFK, afkTimerMicros);
                hearthstone();
                break;
            case(0x32):
                afkTimer.end();
                enterBG();
                break;
            case(0x33):
                prepareAV();
                emulateAVPlayer();
                movementTimer.end();
                movementTimer.begin(randomCommand, movementTimerMicros);
                break;
            case(0x34):
                movementTimer.end();
                leaveBG();
                break;
            case(0x35):
                movementTimer.end();
                afkTimer.end();
                break;
            case(0x36):
                enterQueueOrgri();
                break;
            case(0x37):
                movementTimer.end();
                getOutOfAVCave();
                Keyboard.press(KEY_E);
                Keyboard.release(KEY_E);
                delay(250);
                Keyboard.press(KEY_NUM_LOCK);
                Keyboard.release(KEY_NUM_LOCK);
                movementTimer.begin(randomCommand, movementTimerMicros);
                break;
            case(0x38):
                releaseSpirit();
                delay(30000);
            default:
               break;
        }
    }
}

void nonAFK() {
    Keyboard.press(KEY_A);
    delay(25);
    Keyboard.release(KEY_A);
    delay(550);
    Keyboard.press(KEY_D);
    delay(25);
    Keyboard.release(KEY_D);
}

void emulateAVPlayer() {
    //delay(15000); 
    getOutOfAVCave();
    goToFieldOfStrife();
}

void prepareAV() {
    Keyboard.press(KEY_8);
    Keyboard.release(KEY_8);
    delay(50);
//poisons
    Keyboard.set_modifier(MODIFIERKEY_SHIFT);
    Keyboard.set_key1(KEY_3);
    Keyboard.send_now();
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
    delay(150);
    Keyboard.press(KEY_4);
    Keyboard.release(KEY_4);
    delay(50);
    Keyboard.press(KEY_7);
    Keyboard.release(KEY_7);
    delay(5000);
    Keyboard.press(KEY_3);
    Keyboard.release(KEY_3);
    delay(50);
    Keyboard.press(KEY_8);
    Keyboard.release(KEY_8);
    delay(5000);
    Keyboard.set_modifier(MODIFIERKEY_SHIFT);
    Keyboard.set_key1(KEY_1);
    Keyboard.send_now();
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
}

void getOutOfAVCave() {
    Mouse.moveTo(875, 242);
    delay(100);
    Mouse.click();
    delay(50);
    Keyboard.press(KEY_A);
    delay(255);
    Keyboard.release(KEY_A);
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    delay(2500);
    Keyboard.press(KEY_D);
    delay(290);
    Keyboard.release(KEY_D);
    delay(14500);
}

void goToFieldOfStrife() {
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    Keyboard.press(KEY_D);
    delay(485);
    Keyboard.release(KEY_D);
    Keyboard.set_modifier(MODIFIERKEY_SHIFT);
    Keyboard.set_key1(KEY_T);
    Keyboard.send_now();
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
    delay(4000);
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    delay(15000);
    Mouse.moveTo(960, 540);
    Mouse.set_buttons(0, 0, 1);
    for(int i=0; i<7; i++) {
          Keyboard.set_key1(KEY_A);
          Keyboard.set_key2(KEY_SPACE);
          Keyboard.send_now();
          Keyboard.set_key1(0);
          Keyboard.set_key2(0);
          Keyboard.send_now();
          delay(1000);
          Keyboard.set_key1(KEY_D);
          Keyboard.set_key2(KEY_SPACE);
          Keyboard.send_now();
          Keyboard.set_key1(0);
          Keyboard.set_key2(0);
          Keyboard.send_now();
          delay(1000);
    }
    Mouse.set_buttons(0, 0, 0);
    delay(500);
    Keyboard.press(KEY_E);
    Keyboard.release(KEY_E);
    delay(1000);
    Keyboard.press(KEY_E);
    Keyboard.release(KEY_E);
    delay(100);
    turnRight();
    stealthForward();
}

void releaseSpirit() {
    Mouse.moveTo(960, 225);
    Mouse.click();
}

void enterQueueOrgri() {
    Mouse.moveTo(960, 540);
    Mouse.click();
    Keyboard.set_modifier(MODIFIERKEY_SHIFT);
    Keyboard.set_key1(KEY_3);
    Keyboard.send_now();
    delay(50);
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
    delay(250);
    Keyboard.press(KEY_9);
    Keyboard.release(KEY_9);
    delay(250);
    Keyboard.set_modifier(MODIFIERKEY_CTRL);
    Keyboard.set_key1(KEY_Q);
    Keyboard.send_now();
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
    delay(500);
    Mouse.moveTo(960, 540);
    Mouse.click();
    delay(150);
    Mouse.moveTo(138, 330);
    Mouse.click();
    Keyboard.set_modifier(MODIFIERKEY_SHIFT);
    Keyboard.set_key1(KEY_1);
    Keyboard.send_now();
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
}

void enterQueue() {
    Mouse.moveTo(960, 540);
    Mouse.click();
    delay(150);
    Mouse.moveTo(260, 665);
    delay(100);
    Mouse.click();
}

void enterBG() {
    Mouse.moveTo(875, 242);
    delay(100);
    Mouse.click();
    delay(150);
    Mouse.click();
}

void leaveBG() {
    Mouse.moveTo(970, 755);
    delay(100);
    Mouse.click();
    delay(150);
    Mouse.click();
    delay(150);
}


void turnRight() {
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    Mouse.moveTo(960, 540);
    delay(250);
    Mouse.set_buttons(0, 0, 1); 
    delay(250);
    Mouse.moveTo(1020, 540);
    delay(250);
    Mouse.moveTo(1120, 540);
    delay(250);
    Mouse.moveTo(1220, 540);
    delay(3800);
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    Mouse.set_buttons(0, 0, 0);
    delay(150);
}

void turnLeft() {
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    Mouse.moveTo(960, 540);
    delay(250);
    Mouse.set_buttons(0, 0, 1); 
    delay(250);
    Mouse.moveTo(800, 540);
    delay(250);
    Mouse.moveTo(750, 540);
    delay(250);
    Mouse.moveTo(700, 540);
    delay(3800);
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    Mouse.set_buttons(0, 0, 0);
    delay(150);
}

void jump() {
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    delay(150);
    Keyboard.press(KEY_SPACE);
    Keyboard.release(KEY_SPACE);
    delay(4800);
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
}

void moveForward() {
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
    delay(4900);
    Keyboard.press(KEY_NUM_LOCK);
    Keyboard.release(KEY_NUM_LOCK);
}

void stealthForward() {
    Keyboard.press(KEY_W);
    delay(150);
    Keyboard.press(KEY_E);
    Keyboard.release(KEY_E);
    delay(150);
    Keyboard.press(KEY_E);
    Keyboard.release(KEY_E);
    delay(4500);
    Keyboard.release(KEY_W);
}

void randomCommand() {
    long comm = random(1, 11);
    switch(comm) {
        case(1):
            moveForward();
            break;
        case(2):
            moveForward();
            break;
        case(3): 
            turnRight();
            break;
        case(4):
            turnRight();
            break;
        case(5):
            turnRight();
            break;
        case(6):
            turnLeft();
            break;
        case(7):
            turnLeft();
            break;
        case(8):
            stealthForward();
            break;
        default:
            jump();
    }
}

void hearthstone() {
    Keyboard.set_modifier(MODIFIERKEY_CTRL);
    Keyboard.set_key1(KEY_6);
    Keyboard.send_now();
    Keyboard.set_modifier(0);
    Keyboard.set_key1(0);
    Keyboard.send_now();
}
