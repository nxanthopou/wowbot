#####################################################################
# WoW AV Bot                                                        #
# Author: Nikitas Xanthopoulos                                      #
# Heavily modified OCR https://github.com/ninthwalker/BGNotifier    #
#####################################################################

using namespace Windows.Storage
using namespace Windows.Graphics.Imaging

$path = "C:\Users\monmon\Desktop\bgnot\screenshots"

$joinQueueChars = "*The territorial Frostwolves*" #queue for bg
$joinBattleChars = "*Enter*" #queue popped
$leaveBGChars = "*Wins*" #bg end
$gyPlayerChars = "*Resurrection*" #dead player
$deadPlayerChars = "*elease*" #dead player {R/r}elease

$topLeftQueueEnterX     = 140
$topLeftQueueEnterY     = 570
$bottomRightQueueEnterX = 440
$bottomRightQueueEnterY = 680

$topLeftQueuePopX     = 780
$topLeftQueuePopY     = 200
$bottomRightQueuePopX = 1150
$bottomRightQueuePopY = 300

$topLeftBGEndX     = 900
$topLeftBGEndY     = 750
$bottomRightBGEndX = 1080
$bottomRightBGEndY = 765

$topLeftDeadX = 800
$topLeftDeadY = 150
$bottomRightDeadX = 1150
$bottomRightDeadY = 250

$topLeftReleaseSpiritX = 870
$topLeftReleaseSpiritY = 180
$bottomRightReleaseSpiritX = 1060
$bottomRightReleaseSpiritY = 250

$queueEnterSerialID = 0x01
$avEnterSerialID = 0x02
$emuPlayerAVSerialID = 0x03
$leaveAVSerialID = 0x04
$killTimersSerialID = 0x05
$orgriQueueEnterSerialID = 0x06
$gyPlayerSerialID = 0x07
$releasePlayerSerialID = 0x08

# clicks "Join Battle" in AV master/portal
function Join-Queue {
    $port.WriteLine($queueEnterSerialID)
}

# targets kartra through a key-binded macro
# interracts with her also with a key-binded macro
# clicks "I would like to go to the battleground"
# Join-Queue should take care of rest
function Join-Orgri-Queue {
    $port.WriteLine($orgriQueueEnterSerialID)
}

# Clicks "Enter Battle" when message pops
function Queue-Popped {
    $port.WriteLine($avEnterSerialID)
}

function Emulate-Player-AV {
    $port.WriteLine($emuPlayerAVSerialID)
}

function Leave-AV {
    $port.WriteLine($leaveAVSerialID)
}

function Shut-Timers {
    $port.WriteLine($killTimersSerialID)
}

function Get-Out-Of-AV-Cave {
    $port.WriteLine($gyPlayerSerialID)
}

function Release-Spirit {
    $port.WriteLine($releasePlayerSerialID)
}


Add-Type -AssemblyName System.Drawing
Add-Type -AssemblyName System.Runtime.WindowsRuntime

$null = [Windows.Storage.StorageFile,                Windows.Storage,         ContentType = WindowsRuntime]
$null = [Windows.Media.Ocr.OcrEngine,                Windows.Foundation,      ContentType = WindowsRuntime]
$null = [Windows.Foundation.IAsyncOperation`1,       Windows.Foundation,      ContentType = WindowsRuntime]
$null = [Windows.Graphics.Imaging.SoftwareBitmap,    Windows.Foundation,      ContentType = WindowsRuntime]
$null = [Windows.Storage.Streams.RandomAccessStream, Windows.Storage.Streams, ContentType = WindowsRuntime]


$port = New-Object System.IO.Ports.SerialPort COM10, 115200
$port.Open()
$running = $true

function Take-Screenshot {
    param (
         [parameter (Mandatory=$true)]
          [string []] $filePath,
         [parameter (Mandatory=$true)]
          [int] $topLeftX,
         [parameter (Mandatory=$true)]
          [int] $topLeftY,
         [parameter (Mandatory=$true)]
          [int] $bottomRightX,
         [parameter (Mandatory=$true)]
          [int] $bottomRightY
    )
    $bounds   = [Drawing.Rectangle]::FromLTRB($topLeftX, $topLeftY, $bottomRightX, $bottomRightY)
    $pic      = New-Object System.Drawing.Bitmap ([int]$bounds.width), ([int]$bounds.height)
    $graphics = [Drawing.Graphics]::FromImage($pic)
    $graphics.CopyFromScreen($bounds.Location, [Drawing.Point]::Empty, $bounds.size)
    $pic.Save("$filePath")
    $graphics.Dispose()
    $pic.Dispose()
}

function Scan-Screen {
    Take-Screenshot "$path\joinQueue.png" $topLeftQueueEnterX $topLeftQueueEnterY $bottomRightQueueEnterX $bottomRightQueueEnterY
    Take-Screenshot "$path\queuePopped.png" $topLeftQueuePopX $topLeftQueuePopY $bottomRightQueuePopX $bottomRightQueuePopY
    Take-Screenshot "$path\bgEnded.png" $topLeftBGEndX $topLeftBGEndY $bottomRightBGEndX $bottomRightBGEndY
    Take-Screenshot "$path\graveyard.png" $topLeftDeadX $topLeftDeadY $bottomRightDeadX $bottomRightDeadY
    Take-Screenshot "$path\releaseSpirit.png" $topLeftReleaseSpiritX $topLeftReleaseSpiritY $bottomRightReleaseSpiritX $bottomRightReleaseSpiritY

    $queueEnter = (Get-OCR "$path\joinQueue.png").Text
    $bgAlert = (Get-OCR "$path\queuePopped.png").Text
    $bgEnded = (Get-OCR "$path\bgEnded.png").Text
    $gyPlayer = (Get-OCR "$path\graveyard.png").Text
    $releaseSpirit = (Get-OCR "$path\releaseSpirit.png").Text

    #Write-Host "$releaseSpirit"
    if ($releaseSpirit -like $deadPlayerChars) {
        $timeNow =  Get-Date -DisplayHint Time
        Write-Host "$timeNow : Dead player"
        Release-Spirit
        Sleep -Milliseconds 500
    }
    
    if ($gyPlayer -like $gyPlayerChars) {
        $timeNow =  Get-Date -DisplayHint Time
        Write-Host "$timeNow : GY "
        Sleep -Seconds 35 # give time to rez
        Get-Out-Of-AV-Cave
    }
    

    if ($queueEnter -like $joinQueueChars) {
        $timeNow =  Get-Date -DisplayHint Time
        Write-Host "$timeNow : Joined Queue"
        Join-Queue
        Sleep -Seconds 5
    }

    if ($bgAlert -like $joinBattleChars) {
        $timeNow =  Get-Date -DisplayHint Time
        Write-Host "$timeNow : Queue Popped"
        Queue-Popped
        Sleep -Seconds 120
        Emulate-Player-AV
    } 

    if ($bgAlert -like $leaveBGChars) {
        $timeNow =  Get-Date -DisplayHint Time
        Write-Host "$timeNow : BG ended"
        Shut-Timers
        Sleep -Seconds 60
        Leave-AV
        Sleep -Seconds 45
        Join-Orgri-Queue
    }
}

function Get-OCR {
# Takes a path to an image file, with some text on it.
# Runs Windows 10 OCR against the image.
# Returns an [OcrResult], hopefully with a .Text property containing the text
# OCR part of the script from: https://github.com/HumanEquivalentUnit/PowerShell-Misc/blob/master/Get-Win10OcrTextFromImage.ps1
    [CmdletBinding()]
    Param
    (
        # Path to an image file
        [Parameter(Mandatory=$true, 
                    ValueFromPipeline=$true,
                    ValueFromPipelineByPropertyName=$true, 
                    Position=0,
                    HelpMessage='Path to an image file, to run OCR on')]
        [ValidateNotNullOrEmpty()]
        $Path
    )

    Begin {
        # [Windows.Media.Ocr.OcrEngine]::AvailableRecognizerLanguages
        $ocrEngine = [Windows.Media.Ocr.OcrEngine]::TryCreateFromUserProfileLanguages()

        # PowerShell doesn't have built-in support for Async operations, 
        # but all the WinRT methods are Async.
        # This function wraps a way to call those methods, and wait for their results.
        $getAwaiterBaseMethod = [WindowsRuntimeSystemExtensions].GetMember('GetAwaiter').
                                    Where({
                                            $PSItem.GetParameters()[0].ParameterType.Name -eq 'IAsyncOperation`1'
                                        }, 'First')[0]

        Function Await {
            param($AsyncTask, $ResultType)

            $getAwaiterBaseMethod.
                MakeGenericMethod($ResultType).
                Invoke($null, @($AsyncTask)).
                GetResult()
        }
    }

    Process {
        foreach ($p in $Path) {
      
            # From MSDN, the necessary steps to load an image are:
            # Call the OpenAsync method of the StorageFile object to get a random access stream containing the image data.
            # Call the static method BitmapDecoder.CreateAsync to get an instance of the BitmapDecoder class for the specified stream. 
            # Call GetSoftwareBitmapAsync to get a SoftwareBitmap object containing the image.
            #
            # https://docs.microsoft.com/en-us/windows/uwp/audio-video-camera/imaging#save-a-softwarebitmap-to-a-file-with-bitmapencoder

            # .Net method needs a full path, or at least might not have the same relative path root as PowerShell
            $p = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($p)
        
            $params = @{ 
                AsyncTask  = [StorageFile]::GetFileFromPathAsync($p)
                ResultType = [StorageFile]
            }
            $storageFile = Await @params

            $params = @{ 
                AsyncTask  = $storageFile.OpenAsync([FileAccessMode]::Read)
                ResultType = [Streams.IRandomAccessStream]
            }
            $fileStream = Await @params

            $params = @{
                AsyncTask  = [BitmapDecoder]::CreateAsync($fileStream)
                ResultType = [BitmapDecoder]
            }
            $bitmapDecoder = Await @params

            $params = @{ 
                AsyncTask = $bitmapDecoder.GetSoftwareBitmapAsync()
                ResultType = [SoftwareBitmap]
            }
            $softwareBitmap = Await @params

            Await $ocrEngine.RecognizeAsync($softwareBitmap) ([Windows.Media.Ocr.OcrResult])

        }
    }
}
$timeNow =  Get-Date -DisplayHint Time
Write-Host "$timeNow : Started script"
do {
    Scan-Screen
    if ([console]::KeyAvailable) {
        $key = [system.console]::readkey($true)
        if ($key.key -eq "q") {
            $running = $false
        }
    }
    #$CallStack = Get-PSCallStack | Select-Object -Property *
    #Write-Host $CallStack.Count
} while ($running -eq $true)

Shut-Timers
$port.Close()



