# Information

> ***Do not use this in official World of Warcraft servers, "botting" is against Blizzard's Terms of Services.***

A very basic bot for the famous World of Warcraft MMORPG game. It is dedicated on honor farming. I made it just for the fun, hence the ugly code base and hardcoded parameters.

# Components

A script in powershell takes screenshots of the display and crops them in specific locations. Windows' embedded OCR (Optical Character Recognition) algorithms extract text from the latter.  
When they match predifined strings, Teensy 3.6 (ARM Cortex M4) is commanded to emulate keyboard and mouse actions.

The player is randomly moving across WoW's Alterac Valley battleground, to leech some honor and not get kicked because of AFK'ing.


# Disclaimers

I have only run this software while being in front of the computer (not-AFK) and for research purposes ;).  
Powershell script is a heavily modified version of https://github.com/ninthwalker/BGNotifier

# Videos

